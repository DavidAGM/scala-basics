package davidagm
package exercises

import scala.annotation.tailrec
import scala.collection.MapView

// This exercise is of my own. I wanted to recreate the behaviours analyzed in the lesson TuplesAndMaps
object MiniSocialNetwork {

  def main(args: Array[String]): Unit = {
    // === Testing the class ===

    println(" ===  Empty === ")
    val empty: SocialNetwork = EmptyNetwork
    println(empty)
    println(empty.mostFriends)
    println(empty.nPeopleWithNoFriends)

    println(" === Basic Network Ops === ")
    val network: SocialNetwork = empty.addMember(Map("Bob" -> Set[Person]())).addMember("Mary")
    val bobAndMary = network.addFriend("Bob", "Mary")
    println(network)
    println(bobAndMary)
    println(bobAndMary.unFriend("Bob", "Mary"))
    println(bobAndMary.remove("Bob"))

    println(" === Two Network Constructors === ")
    val testNet: SocialNetwork = bobAndMary.addMember("Jim").addFriend("Jim", "Bob")
    val testNet2: SocialNetwork = {
      empty.addMember(Map("Bob" -> Set("Mary, Jim")))
        .addMember(Map("Mary" -> Set("Bob")))
        .addMember(Map("Jim" -> Set("Bob")))
    }
    println(testNet)
    println(testNet2)

    println(" === Advanced Network Ops === ")
    println(testNet.nFriend("Bob"))
    println(testNet.mostFriends)
    println(testNet.nPeopleWithNoFriends)
    println(testNet.unFriend("Jim", "Bob").nPeopleWithNoFriends)
    println(testNet.socialConnection("Mary", "Jim"))
  }

  type Person = String
  type Friends = Set[Person]
  type peopleData = Map[Person, Friends]


  abstract class SocialNetwork {
    def isEmpty: Boolean

    def addMember(other: peopleData): SocialNetwork

    def addMember(other: Person): SocialNetwork

    def addFriend(a: Person, b: Person): SocialNetwork

    def unFriend(a: Person, b: Person): SocialNetwork

    def remove(a: Person): SocialNetwork

    def nFriend(a: Person): Int

    def mostFriends: Person

    def nPeopleWithNoFriends: Int

    def socialConnection(a: Person, b: Person): Boolean
  }


  case class Network(peopleData: peopleData) extends SocialNetwork {
    def isEmpty: Boolean = false

    def addMember(other: peopleData): SocialNetwork = Network(peopleData ++ other)

    def addMember(other: Person): SocialNetwork = Network(peopleData ++ Map(other -> Set[Person]()))

    def addFriend(a: Person, b: Person): SocialNetwork = {
      Network(peopleData + (a -> (peopleData(a) + b)) + (b -> (peopleData(b) + a)))
    }

    def unFriend(a: Person, b: Person): SocialNetwork = {
      Network(peopleData + (a -> (peopleData(a) - b)) + (b -> (peopleData(b) - a)))
    }

    def remove(a: Person): SocialNetwork = {
      val friendsOfA: Friends = peopleData(a)
      if (friendsOfA.isEmpty) Network(peopleData - a)
      else Network(peopleData ++ friendsOfA.map((p: Person) => p -> (peopleData(p) - a)) - a)
    }

    def nFriend(a: Person): Int = if (peopleData.contains(a)) peopleData(a).size else 0

    def mostFriends: Person = peopleData.maxBy(_._2.size)._1

    def nPeopleWithNoFriends: Int = peopleData.count(_._2.isEmpty)

    def socialConnection(a: Person, b: Person): Boolean = {
      @tailrec
      def bfs(target: Person, consideredPeople: Friends, discoveredPeople: Friends): Boolean = {
        if (discoveredPeople.isEmpty) false
        else {
          val person: Person = discoveredPeople.head
          if (person == target) true
          else if (consideredPeople.contains(person)) bfs(target, consideredPeople, discoveredPeople.tail)
          else bfs(target, consideredPeople + person, discoveredPeople.tail ++ peopleData(person))
        }
      }

      bfs(b, Set(), peopleData(a) + a)
    }
  }


  object EmptyNetwork extends SocialNetwork {
    def isEmpty: Boolean = true

    def addMember(other: peopleData): SocialNetwork = Network(other)

    def addMember(other: Person): SocialNetwork = Network(Map(other -> Set[Person]()))

    def addFriend(a: Person, b: Person): SocialNetwork = this

    def unFriend(a: Person, b: Person): SocialNetwork = this

    def remove(a: Person): SocialNetwork = this

    def nFriend(a: Person): Int = 0

    def mostFriends: Person = "Empty Network"

    def nPeopleWithNoFriends: Int = 0

    def socialConnection(a: Person, b: Person): Boolean = false
  }

}
