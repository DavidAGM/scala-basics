package davidagm
package exercises

// The collection with just one element seems not to be os use but it is extremely important in handling Options

abstract class Maybe[+T] {

  def map[B](transformer: T => B): Maybe[B]

  def flatMap[B](transformer: T => Maybe[B]): Maybe[B]

  def filter(predicate: T => Boolean): Maybe[T]

}

case object MaybeNot extends Maybe[Nothing] {
  def map[B](transformer: Nothing => B): Maybe[B] = this

  def flatMap[B](transformer: Nothing => Maybe[B]): Maybe[B] = this

  def filter(predicate: Nothing => Boolean): Maybe[Nothing] = this
}

case class Just[+T](value: T) extends Maybe[T]{
  def map[B](transformer: T => B): Maybe[B] = Just(transformer(value))

  def flatMap[B](transformer: T => Maybe[B]): Maybe[B] =transformer(value)

  def filter(predicate: T => Boolean): Maybe[T] =
    if (predicate(value)) this
    else MaybeNot
}

object MaybeTest extends App {
  val some = Just('a')

  println(some.filter((c: Char) => c == 'b'))
  println(some.filter((c: Char) => c == 'a'))

  println(some.map((c: Char) => c.toUpper))
  println(some.flatMap((c: Char) => Just(c.byteValue)))

}

