package davidagm
package exercises

abstract class MyList {

  def head: Int
  def tail: MyList
  def isEmpty: Boolean
  def add(i:Int): MyList
  def printElements: String
  //polymorphic call
  override def toString: String = s"[$printElements]"

}

/*
  head => Int
  tail => List[Int]
  isEmpty => Boolean
  add(i:Int) => new List(list:+i)
  toString => string representation of the list
   */

class Cons(val head: Int, val tail: MyList) extends MyList {
  def isEmpty = false
  def add(i: Int): Cons = new Cons(i, this)
  def printElements: String = s"$head, ${tail.printElements}"
}

object Empty extends MyList {
  def head: Int = throw new NoSuchElementException
  def tail: MyList = throw new NoSuchElementException
  def isEmpty = true
  def add(i: Int) = new Cons(i, Empty)
  def printElements: String = "Nil"
}

object ListTest extends App {
  val list = new Cons(1, new Cons(2, new Cons(3, Empty)))
  println(list.tail.head)
  println(list.add(4).head)
  println(list.toString)
  println(Empty.toString)
}