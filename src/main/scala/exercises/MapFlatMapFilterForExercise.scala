package davidagm
package exercises

import scala.annotation.tailrec

object MapFlatMapFilterForExercise extends App {

  abstract class MyCollection[+T] {

    def fold[B](startValue: B)(function: (B, T) => B): B

    def zipWith[B, C](other: MyCollection[B], zipperFunction: (T, B) => C): MyCollection[C]

    def sort(compare: (T, T) => Int): MyCollection[T]

    def foreach(function: T => Unit): Unit

    def head: T

    def tail: MyCollection[T]

    def isEmpty: Boolean

    def add[B >: T](member: B): MyCollection[B]

    def map[B](transformer: T => B): MyCollection[B]

    def filter(predicate: T => Boolean): MyCollection[T]

    def flatMap[B](transformer: T => MyCollection[B]): MyCollection[B]

    def ++[B >: T](other: MyCollection[B]): MyCollection[B]

    def printElements: String

    override def toString: String = s"[$printElements]"
  }

  class MyCollectionCons[T](val head: T, val tail: MyCollection[T]) extends MyCollection[T] {

    def isEmpty: Boolean = false

    def add[B >: T](member: B): MyCollectionCons[B] = new MyCollectionCons[B](member, this)

    def map[B](transformer: T => B): MyCollection[B] = new MyCollectionCons[B](transformer(head), tail.map(transformer))

    def filter(predicate: T => Boolean): MyCollection[T] = {
      if (predicate(head)) new MyCollectionCons[T](head, tail.filter(predicate))
      else tail.filter(predicate)
    }

    def flatMap[B](transformer: T => MyCollection[B]): MyCollection[B] = transformer(head) ++ tail.flatMap(transformer)

    def ++[B >: T](other: MyCollection[B]): MyCollection[B] = new MyCollectionCons[B](head, tail ++ other)

    def printElements: String = s"$head, ${tail.printElements}"

    def fold[B](startValue: B)(function: (B, T) => B): B = tail.fold(function(startValue, head))(function)

    def zipWith[B, C](other: MyCollection[B], zipperFunction: (T, B) => C): MyCollection[C] = {
      if (other.isEmpty) throw new RuntimeException("List do not have the same number of fields!")
      else new MyCollectionCons[C](zipperFunction(head, other.head), tail.zipWith(other.tail, zipperFunction))
    }

    def sort(compare: (T, T) => Int): MyCollection[T] = {
      def insert(element: T, sortedList: MyCollection[T]): MyCollectionCons[T] = {
        if (sortedList.isEmpty) new MyCollectionCons[T](element, MyCollectionEmpty)
        else if (compare(element, sortedList.head) <= 0) new MyCollectionCons[T](element, sortedList)
        else new MyCollectionCons[T](sortedList.head, insert(element, sortedList.tail))
      }

      val sortedTail = tail.sort(compare)
      insert(head, sortedTail)
    }

    def foreach(function: T => Unit): Unit = {
      function(head)
      tail.foreach(function)
    }
  }

  object MyCollectionEmpty extends MyCollection[Nothing] {
    def head: Nothing = throw new NoSuchFieldError

    def tail: MyCollection[Nothing] = throw new NoSuchFieldError

    def add[B >: Nothing](member: B): MyCollectionCons[B] = new MyCollectionCons[B](member, this)

    def isEmpty: Boolean = true

    def filter(predicate: Nothing => Boolean): MyCollection[Nothing] = this

    def map[B](transformer: Nothing => B): MyCollection[B] = this

    def flatMap[B](transformer: Nothing => MyCollection[B]): MyCollection[B] = this

    def ++[B >: Nothing](other: MyCollection[B]): MyCollection[B] = other

    def printElements: String = ""

    def fold[B](startValue: B)(function: (B, Nothing) => B): B = startValue

    def zipWith[B, C](other: MyCollection[B], zipperFunction: (Nothing, B) => C): MyCollection[C] = MyCollectionEmpty

    def sort(compare: (Nothing, Nothing) => Int): MyCollection[Nothing] = MyCollectionEmpty

    def foreach(function: Nothing => Unit): Unit = ()

  }

  /* Does this list support for-comprehensions? */
  val someCollectionOfNumbers = new MyCollectionCons[Short](5,
    new MyCollectionCons[Short](4,
      new MyCollectionCons[Short](3,
        new MyCollectionCons[Short](2,
          new MyCollectionCons[Short](1, MyCollectionEmpty)))))

  val negativeCollection = for {
    c <- someCollectionOfNumbers
  } yield c * -1

  println(negativeCollection.toString)

  // filter does not work in for-comprehension because is actually a withFilter call, not a filter call
  //for {
  //  nc <- negativeCollection if nc % 2 == 0
  //} println(nc)

  // using a filter call directly in the root collection does work indeed
  for {
    nc <- negativeCollection.filter(_ % 2 == 0)
  } println(nc)


  //======================= Colección con un solo elemento =======================


  class MyOneElementCollection[T](val head: T, val tail: MyCollection[T]) extends MyCollection[T] {

    def isEmpty: Boolean = false

    def add[B >: T](member: B): MyOneElementCollection[B] = new MyOneElementCollection[B](member, MyOneElementCollectionEmpty)

    def map[B](transformer: T => B): MyCollection[B] = new MyOneElementCollection[B](transformer(head), MyOneElementCollectionEmpty.map(transformer))

    def filter(predicate: T => Boolean): MyCollection[T] = {
      if (predicate(head)) new MyOneElementCollection[T](head, MyOneElementCollectionEmpty.filter(predicate))
      else MyOneElementCollectionEmpty.filter(predicate)
    }

    def flatMap[B](transformer: T => MyCollection[B]): MyCollection[B] = transformer(head) ++ MyOneElementCollectionEmpty.flatMap(transformer)

    def ++[B >: T](other: MyCollection[B]): MyCollection[B] = new MyOneElementCollection[B](head, MyOneElementCollectionEmpty ++ other)

    def printElements: String = s"$head, ${MyOneElementCollectionEmpty.printElements}"

    def fold[B](startValue: B)(function: (B, T) => B): B = tail.fold(function(startValue, head))(function)

    def zipWith[B, C](other: MyCollection[B], zipperFunction: (T, B) => C): MyCollection[C] = {
      if (other.isEmpty) throw new RuntimeException("List do not have the same number of fields!")
      else new MyOneElementCollection[C](zipperFunction(head, other.head), tail.zipWith(MyOneElementCollectionEmpty, zipperFunction))
    }

    def sort(compare: (T, T) => Int): MyCollection[T] = {
      def insert(element: T, sortedList: MyCollection[T]): MyOneElementCollection[T] = {
        if (sortedList.isEmpty) new MyOneElementCollection[T](element, MyCollectionEmpty)
        else if (compare(element, sortedList.head) <= 0) new MyOneElementCollection[T](element, sortedList)
        else new MyOneElementCollection[T](sortedList.head, insert(element, MyOneElementCollectionEmpty))
      }

      val sortedTail = MyOneElementCollectionEmpty.sort(compare)
      insert(head, sortedTail)
    }

    def foreach(function: T => Unit): Unit = {
      function(head)
      MyOneElementCollectionEmpty.foreach(function)
    }
  }

  object MyOneElementCollectionEmpty extends MyCollection[Nothing] {
    def head: Nothing = throw new NoSuchFieldError

    def tail: MyCollection[Nothing] = throw new NoSuchFieldError

    def add[B >: Nothing](member: B): MyOneElementCollection[B] = new MyOneElementCollection[B](member, this)

    def isEmpty: Boolean = true

    def filter(predicate: Nothing => Boolean): MyCollection[Nothing] = this

    def map[B](transformer: Nothing => B): MyCollection[B] = this

    def flatMap[B](transformer: Nothing => MyCollection[B]): MyCollection[B] = this

    def ++[B >: Nothing](other: MyCollection[B]): MyCollection[B] = other

    def printElements: String = ""

    def fold[B](startValue: B)(function: (B, Nothing) => B): B = startValue

    def zipWith[B, C](other: MyCollection[B], zipperFunction: (Nothing, B) => C): MyCollection[C] = this

    def sort(compare: (Nothing, Nothing) => Int): MyCollection[Nothing] = this

    def foreach(function: Nothing => Unit): Unit = ()

  }

  val someOneElementList = new MyOneElementCollection[Int](1, MyOneElementCollectionEmpty)
  val otherOneElementList = new MyOneElementCollection[Int](2, MyOneElementCollectionEmpty)

  val sum: MyCollection[Int] = someOneElementList ++ otherOneElementList
  println(sum.toString)

  val sumMap = for {
    s <- sum
    string <- sum.map((i: Int) => s"$i")
  } yield Map[Int, String](s -> string)

  println(sumMap)
}
