package davidagm
package exercises

import scala.annotation.tailrec

object ExpandedCollection extends App {

  /*
  1) Generic trait MyPredicate[T]
  2) Generic Trait MyTransformer[A, B]
  3) MyList:
      - map(transformer) => MyList
      - filter(predicate) => MyList
      - flatMap(transformer[A, MyList[B]) => MyList[B]
   */

  val empty = MyExpandedEmpty

  trait MyPredicate[-T] {
    def test(element: T): Boolean
  }

  trait MyTransformer[-A, B] {
    def transform(element: A): B
  }

  abstract class MyExpandedList[+A] {
    def head: A

    def tail: MyExpandedList[A]

    def isEmpty: Boolean

    def add[B >: A](member: B): MyExpandedList[B]

    def map[B](transformer: MyTransformer[A, B]): MyExpandedList[B]

    def filter(predicate: MyPredicate[A]): MyExpandedList[A]

    def flatMap[B](transformer: MyTransformer[A, MyExpandedList[B]]): MyExpandedList[B]

    def ++[B >: A](other: MyExpandedList[B]): MyExpandedList[B]

    def printElements: String
    override def toString: String = s"[$printElements]"
  }

  class MyExpandedCons[A](val head: A, val tail: MyExpandedList[A]) extends MyExpandedList[A] {

    def isEmpty: Boolean = false

    def add[B >: A](member: B): MyExpandedCons[B] = new MyExpandedCons[B](member, this)

    def transform[B <: A]: B = head.asInstanceOf[B]

    def map[B](transformer: MyTransformer[A, B]): MyExpandedList[B] = {
      new MyExpandedCons[B](transformer.transform(head), tail.map(transformer))
    }

    def filter(predicate: MyPredicate[A]): MyExpandedList[A] = {
      if (predicate.test(head)) new MyExpandedCons[A](head, tail.filter(predicate))
      else tail.filter(predicate)
    }

    def flatMap[B](transformer: MyTransformer[A, MyExpandedList[B]]): MyExpandedList[B] = {
      transformer.transform(head) ++ tail.flatMap(transformer)
    }


    // My implementation also works
    //def ++[B >: A](other: MyExpandedList[B]): MyExpandedList[B] = {
    //  if (other.isEmpty) this
    //  else this.add(other.head).++(other.tail)
    //}
    //Daniel Implementation
    def ++[B >: A](other: MyExpandedList[B]): MyExpandedList[B] = {
      new MyExpandedCons[B](head, tail ++ other)
    }
    def printElements: String = s"$head, ${tail.printElements}"

  }

  object MyExpandedEmpty extends MyExpandedList[Nothing] {
    def head: Nothing = throw new NoSuchFieldError
    def tail: MyExpandedList[Nothing] = throw new NoSuchFieldError
    def add[B >: Nothing](member: B): MyExpandedCons[B] = new MyExpandedCons[B](member, this)
    def isEmpty: Boolean = true
    def filter(predicate: MyPredicate[Nothing]): MyExpandedList[Nothing] = this
    def map[B](transformer: MyTransformer[Nothing, B]): MyExpandedList[B] = this
    def flatMap[B](transformer: MyTransformer[Nothing, MyExpandedList[B]]): MyExpandedList[B] = this
    def ++[B >: Nothing](other: MyExpandedList[B]): MyExpandedList[B] = other
    def printElements: String = ""

  }
  println(empty.add("jaja").printElements)
  println(empty.add(1).printElements)

  println(new MyExpandedCons(1, new MyExpandedCons(2, MyExpandedEmpty)).filter(new MyPredicate[Int] {
    def test(element: Int): Boolean = element % 2 == 0
  }).printElements)

  println(empty.add("abc").add("def").map[String](new MyTransformer[String, String] {
    def transform(element: String): String = element.toUpperCase
  }).printElements)

  println((empty++new MyExpandedCons[Int](10, empty)).printElements)

  val oneList = new MyExpandedCons[Short](1, new MyExpandedCons[Short](2, new MyExpandedCons[Short](3, MyExpandedEmpty)))
  val anotherList = new MyExpandedCons[Short](100, MyExpandedEmpty)
  println(oneList.printElements)
  println(anotherList.printElements)
  val twoLists = oneList++anotherList
  println(twoLists.printElements)
  println(twoLists.map(short => short * 2).printElements)
  println(twoLists.flatMap(short => new MyExpandedCons[Short](short, new MyExpandedCons[Short](short, MyExpandedEmpty))).printElements)
  println(new MyExpandedCons[Int](1, new MyExpandedCons[Int](2, new MyExpandedCons[Int](3,
    new MyExpandedCons[Int](4, MyExpandedEmpty)))).flatMap(new MyTransformer[Int, MyExpandedList[Int]] {
    def transform(element: Int): MyExpandedList[Int] = {
      new MyExpandedCons[Int](element-1, new MyExpandedCons[Int](element, new MyExpandedCons[Int](element+1, MyExpandedEmpty)))
    }
  }).printElements)


}
