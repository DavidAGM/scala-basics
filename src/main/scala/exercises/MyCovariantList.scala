package davidagm
package exercises

abstract class MyCovariantList[+A] {

  def head: A
  def tail: MyCovariantList[A]
  def isEmpty: Boolean
  def add[B >: A](i: B): MyCovariantList[B]
  def printElements: String
  //polymorphic call
  override def toString: String = s"[$printElements]"

}

/*
  head => Int
  tail => List[Int]
  isEmpty => Boolean
  add(i:Int) => new List(list:+i)
  toString => string representation of the list
   */

class CovariantCons[+A](val head: A, val tail: MyCovariantList[A]) extends MyCovariantList[A] {
  def isEmpty = false
  def add[B >: A](i: B): MyCovariantList[B] = new CovariantCons[B](i, this)
  def printElements: String = s"$head, ${tail.printElements}"
}

object CovariantEmpty extends MyCovariantList[Nothing] {
  def head: Nothing = throw new NoSuchElementException
  def tail: MyCovariantList[Nothing] = throw new NoSuchElementException
  def isEmpty = true
  def add[B >: Nothing](i: B): MyCovariantList[B] = new CovariantCons(i, CovariantEmpty)
  def printElements: String = ""
}

object CovariantListTest extends App {
  val list = {
    new CovariantCons(1,
      new CovariantCons(2,
        new CovariantCons(3,
          CovariantEmpty)))
  }
  println(list.tail.head)
  println(list.add(4).head)
  println(list.toString)
  println(Empty.toString)


  val stringList = {
    new CovariantCons("Hello",
      new CovariantCons(" ",
        new CovariantCons("world",
          CovariantEmpty)))
  }
  println(stringList.tail.head)
  println(stringList.toString)
  println((stringList.add(" ").add("!")).toString)
  println(Empty.toString)

}