package davidagm
package exercises

import scala.annotation.tailrec

object MyFunctionalListExercise extends App {

/*
 Exercises:
 1) Expand MyList:
   - foreach method A=>Unit
   - sort function ((A,A)=>Int) => MyList
   - zipWith (list, (A, A) => B) => MyList[B]
   - fold(start)(function) => value
 */
  val empty = MyFunctionalEmpty

  abstract class MyFunctionalList[+A] {

    def fold[B](startValue: B)(function: (B, A) => B): B

    def zipWith[B, C](other: MyFunctionalList[B], zipperFunction: (A, B) => C): MyFunctionalList[C]

    def sort(compare: (A, A) => Int): MyFunctionalList[A]

    def foreach(function: A => Unit): Unit

    def head: A

    def tail: MyFunctionalList[A]

    def isEmpty: Boolean

    def add[B >: A](member: B): MyFunctionalList[B]

    def map[B](transformer: A => B): MyFunctionalList[B]

    def filter(predicate: A => Boolean): MyFunctionalList[A]

    def flatMap[B](transformer: A => MyFunctionalList[B]): MyFunctionalList[B]

    def ++[B >: A](other: MyFunctionalList[B]): MyFunctionalList[B]

    def printElements: String

    override def toString: String = s"[$printElements]"
  }

  case class MyFunctionalCons[A](head: A, tail: MyFunctionalList[A]) extends MyFunctionalList[A] {

    def isEmpty: Boolean = false

    def add[B >: A](member: B): MyFunctionalCons[B] = new MyFunctionalCons[B](member, this)

    def transform[B <: A]: B = head.asInstanceOf[B]

    def map[B](transformer: A => B): MyFunctionalList[B] = {
      new MyFunctionalCons[B](transformer(head), tail.map(transformer))
    }

    def filter(predicate: A => Boolean): MyFunctionalList[A] = {
      if (predicate(head)) new MyFunctionalCons[A](head, tail.filter(predicate))
      else tail.filter(predicate)
    }

    def flatMap[B](transformer: A => MyFunctionalList[B]): MyFunctionalList[B] = {
      transformer(head) ++ tail.flatMap(transformer)
    }

    def ++[B >: A](other: MyFunctionalList[B]): MyFunctionalList[B] = {
      new MyFunctionalCons[B](head, tail ++ other)
    }

    def printElements: String = s"$head, ${tail.printElements}"

    def fold[B](startValue: B)(function: (B, A) => B): B =
      tail.fold(function(startValue, head))(function)

    def zipWith[B, C](other: MyFunctionalList[B], zipperFunction: (A, B) => C): MyFunctionalList[C] = {
   // Daniel implementation
   if (other.isEmpty) throw new RuntimeException("List do not have the same number of fields!")
   else new MyFunctionalCons[C](zipperFunction(head, other.head), tail.zipWith(other.tail, zipperFunction))
      // This was my implementation, that works btw
      //   @tailrec
   //   def helper(thisList: MyFunctionalList[A], otherList: MyFunctionalList[B], result: MyFunctionalList[C])
   //   : MyFunctionalList[C] =
   //     if (thisList.isEmpty || otherList.isEmpty) result
   //     else helper(thisList.tail, otherList.tail, new MyFunctionalCons[C](zipperFunction(thisList.head, otherList.head), result))
   //   helper(this, other, MyFunctionalEmpty)
    }



    def sort(compare: (A, A) => Int): MyFunctionalList[A] = {
      def insert(element: A, sortedList: MyFunctionalList[A]): MyFunctionalCons[A] =
        if (sortedList.isEmpty) new MyFunctionalCons[A](element, MyFunctionalEmpty)
        else if (compare(element, sortedList.head) <= 0) new MyFunctionalCons[A](element, sortedList)
        else new MyFunctionalCons[A](sortedList.head, insert(element, sortedList.tail))

      val sortedTail = tail.sort(compare)
      insert(head, sortedTail)
    }


    def foreach(function: A => Unit): Unit = {
      function(head)
      tail.foreach(function)
    }
  }

  object MyFunctionalEmpty extends MyFunctionalList[Nothing] {
    def head: Nothing = throw new NoSuchFieldError

    def tail: MyFunctionalList[Nothing] = throw new NoSuchFieldError

    def add[B >: Nothing](member: B): MyFunctionalCons[B] = new MyFunctionalCons[B](member, this)

    def isEmpty: Boolean = true

    def filter(predicate: Nothing => Boolean): MyFunctionalList[Nothing] = this

    def map[B](transformer: Nothing => B): MyFunctionalList[B] = this

    def flatMap[B](transformer: Nothing => MyFunctionalList[B]): MyFunctionalList[B] = this

    def ++[B >: Nothing](other: MyFunctionalList[B]): MyFunctionalList[B] = other

    def printElements: String = ""

    def fold[B](startValue: B)(function: (B, Nothing) => B): B = startValue

    def zipWith[B, C](other: MyFunctionalList[B], zipperFunction: (Nothing, B) => C): MyFunctionalList[C] =
      MyFunctionalEmpty

    def sort(compare: (Nothing, Nothing) => Int): MyFunctionalList[Nothing] = MyFunctionalEmpty


    def foreach(function: Nothing => Unit): Unit = ()

  }

  println(empty.add("jaja").printElements)
  println(empty.add(1).printElements)

  println(new MyFunctionalCons(1, new MyFunctionalCons(2, MyFunctionalEmpty)).filter(_ % 2 == 0)
    .printElements)

  println(empty.add("abc").add("def").map[String](_.toUpperCase).printElements)

  println((empty ++ new MyFunctionalCons[Int](10, empty)).printElements)

  val oneList = new MyFunctionalCons[Short](1,
    new MyFunctionalCons[Short](2, new MyFunctionalCons[Short](3, MyFunctionalEmpty)))
  val anotherList = new MyFunctionalCons[Short](100, MyFunctionalEmpty)
  println(oneList.printElements)
  println(anotherList.printElements)
  val twoLists = oneList ++ anotherList
  println(twoLists.printElements)
  println(twoLists.map(_ * 2).printElements)
  println(twoLists.flatMap(
    short => new MyFunctionalCons[Short](short, new MyFunctionalCons[Short](short, MyFunctionalEmpty))).printElements)
  println(new MyFunctionalCons[Int](1, new MyFunctionalCons[Int](2, new MyFunctionalCons[Int](3,
    new MyFunctionalCons[Int](4, MyFunctionalEmpty)))).flatMap(element => {
    new MyFunctionalCons[Int](element - 1,
      new MyFunctionalCons[Int](element, new MyFunctionalCons[Int](element + 1, MyFunctionalEmpty)))
  }).printElements)

  println("----------- métodos funcionales ------------")
  twoLists.foreach(println(_))
  oneList.sort((x, y) => x - y).foreach(println)

  println(twoLists.sort((x, y) => y - x).toString)

  val doubleOneList = oneList.map(_*2)
  println(oneList.toString)
  println(doubleOneList.toString)

  println(oneList.zipWith(doubleOneList, (x: Short,y: Int) => (x,y)).toString)
  println(oneList.zipWith(doubleOneList, (x: Short,y: Int) => x*y).toString)

  println(oneList.fold(0)(_+_))

}
