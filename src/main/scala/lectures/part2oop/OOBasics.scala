package davidagm
package lectures.part2oop

object OOBasics {

  def main(args: Array[String]): Unit = {

    val person = new Person("Daniel", 40)
    println(person.age)
    println(person.x)
    person.greet("John")
    person.greet()

    val author = new Writer("Charles", "Dickens", 1812)
    val novel = new Novel("Great Expectations", 1861, author)
    val imposter = new Writer("Charles", "Dickens", 1812)

    println(novel.authorAge)
    println(novel.isWrittenBy(author))
    // aunque los datos internos son los mismos, las instancias de las clases no lo son
    println(novel.isWrittenBy(imposter))


    val counter = new Counter()
    counter.print()
    counter.toIncrement(5).print()
    // se resta desde la instancia inicial, no desde la instancia que tiene counter = 5
    counter.toDecrease(2).print()
    // la instancia inicial sigue teniendo el mismo valor
    counter.print()




  }
}

//constructor
class Person(name: String, val age: Int = 0) {
  // body
  val x = 2

  // method
  def greet(name: String): Unit = println(s"${this.name} says: Hi $name")

  //overloading methods
  def greet(): Unit = println(s"Hi, I am $name!")

  //overloading constructor
  //this constructor is of no use if tou can set a default value in the main constructor
  def this(name: String) = this(name, 0)
  def this() = this("John Doe")
}

class Writer(firstName: String, surName: String, val year: Int){
  def fullName: String = s"$firstName $surName"
}
class Novel(name: String, year: Int, author: Writer) {
  def authorAge: Int = year - author.year
  def isWrittenBy(author: Writer): Boolean = author == this.author
  def copy(newYear: Int): Novel = new Novel(name, newYear, author)
}

  class Counter(val count: Int = 0) {
    // immutability
    def toIncrement: Counter = new Counter(count + 1)
    def toDecrease: Counter = new Counter(count - 1)
    // overload
    //def toIncrement(increment: Int) = new Counter(count + increment)
    //def toDecrease(decrease: Int): Counter = new Counter(count - decrease)
    // overload recursively
    def toIncrement(increment: Int): Counter = {
      if (increment <= 0) this
      else toIncrement.toIncrement(increment - 1)
    }
    def toDecrease(decrease: Int): Counter = {
      if(decrease <= 0) this
      else toDecrease.toDecrease(decrease - 1)
    }
    def print(): Unit = println(count)
}
