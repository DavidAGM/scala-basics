package davidagm
package lectures.part2oop

object CaseClasses extends App {

  case class Person(name: String, age: Int)

  val jim = new Person("Jim", 34)

  // 1) class parameters are fields
  println(jim.name)
  // 2) sensible toString
  println(jim.toString)
  println(jim) // launches .toString

  // 3) equal and hasCode for free
  println(jim.hashCode())
  val jim2 = Person("Jim", 34)

  println(jim.equals(jim2))

  // 4) CCs have handy copy method
  val jim3 = jim.copy()
  val john = jim.copy("John")
  println(john)

  // 5) CCs have companion objects
  val thePerson = Person
  val mary = Person("Mary", 21)

  // 6) CCs are serializable:
  //  - Akka

  // 7) CCs have extractor patters => Can be used in Pattern Matching

}
