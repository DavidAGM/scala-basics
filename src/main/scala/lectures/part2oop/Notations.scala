package davidagm
package lectures.part2oop

import scala.language.postfixOps

object Notations {

  val mary = new Person("Mary", "Inception")
  val tom = new Person("Tom", "Fight Club")

  def main(args: Array[String]): Unit = {
    println(mary.likes("Inception"))
    println(mary likes "Inception")

    println(mary + tom)

    println(-1 == 1.unary_-)
    //unary_ prefix only works with +, -, !, ~

    println(mary.unary_!)
    println(!mary)

    println(mary isAlive)
    println(mary.isAlive)


    println(mary.apply())
    println(mary())


    println(mary.age)
    println((+mary).age)

    println((mary+"the Rock Star!")())

    println(mary(4))

    println(mary learnsScala)
  }
  /*
  2) add age to the person class
  add a unary_+ operator => new person age +1
   */
  class Person(val name: String, favouriteMovie: String, val age: Int = 0) {
    def unary_+ : Person = new Person(name, favouriteMovie, age + 1)

    def likes(movie: String): Boolean = movie == favouriteMovie

    //in-fix notation
    def +(person: Person): String = s"${this.name} is hanging out with ${person.name}"
    /*
1) overload the + operator  mary + "the rock star" => new Person Mary (theRockStart)
*/
    def +(nickName: String): Person = new Person(s"$name $nickName", favouriteMovie, age)
    //pre-fix notation
    //Add an space between the unary name operator and the colon for the type so the compiler does not  think the
    // colon is part of the name
    def unary_! : String = s"$name, what the heck?!"

    //post-fix notation
    def isAlive: Boolean = true

    def apply(): String = s"Hi, my name is $name and I like $favouriteMovie"
    /*
     4) Overload the apply method mary.apply(2) => Mary watched their favourite movie 2 times
      */
    def apply(times: Int): String = s"Hi, my name is $name and I have watched $favouriteMovie $times times"
    /*
    3) add a "learns" method in the person class Scala => Mary learns Scala
    add learnsScala method and calls the learns method with scala as parameter. Us it in post fix notation
     */
    def learns(subject: String): String = s"$name is learning $subject"
    def learnsScala: String = learns("Scala")
  }


}

