package davidagm
package lectures.part2oop

object Enums {
  enum Permissions {
    case READ, WRITE, EXUCTE, NONE

    //add fields or methods
    def openDocument(): Unit =
      if(this == READ) println("opening document")
      else println("reading not allowed")
  }

  val somePermissions: Permissions = Permissions.READ


  // constructor args
  enum PermissionsWithBits(bits: Int){
    case READ extends PermissionsWithBits(4) //100
    case WRITE extends PermissionsWithBits(2) // 010
    case EXECUTE extends PermissionsWithBits(1) // 001
    case NONE extends PermissionsWithBits(0) // 000
  }

  object PermissionsWithBits{
    //factory methods
    def fromBits(bits: Int): PermissionsWithBits = ???
  }


  // standard API
  val somePermissionsOrdinal = somePermissions.ordinal
  val allPermisions = PermissionsWithBits.values
  val readAPermission: Permissions = Permissions.valueOf("READ")

  def main(args: Array[String]): Unit = {
    somePermissions.openDocument()
    println(somePermissionsOrdinal)

    println(readAPermission)
  }

}
