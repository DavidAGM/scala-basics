package davidagm
package lectures.part2oop

object AnonymousClasses extends App {

  abstract class Animal {
    def eat(): Unit
  }

  val funnyAnimal : Animal = () => println("ahahahha")

  println(funnyAnimal.eat())
  println(funnyAnimal.getClass)

  class Person(name: String) {
    def sayHi(): Unit = println(s"Hi, my name is $name, how can I help you?")
  }

  val person = new Person("Bobie")
  person.sayHi()

  val jim = new Person("Jim") {
    override def sayHi(): Unit = println("I'm Jim!")
  }
  jim.sayHi()
}
