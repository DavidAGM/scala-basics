package davidagm
package lectures.part2oop

object Exceptions extends App {

  val x: String = null
  //(This will crash)
  //println(x.length)

  // 1) Throwing and catching exceptions

  def aWeirdValue = throw new NullPointerException

  // throwable class extend the throwable class
  // Exception and Erro are the major Throwable subtypes

  // 2) how to catch exceptions?

  def getInt(withExcetion: Boolean): Int = {
    if (withExcetion) throw new RuntimeException("No int for you!")
    else 42
  }

  try {
    getInt(true)
  } catch {
    case r: RuntimeException => println("Caught a runtime exception!")
  } finally {
    // code that will get execute no matter what
    // optional
    // does not influence the return typeof this expression
    //use it for side effects like loggin
    println("finally")
  }

  class MyException extends Exception
  val exception = new MyException

  /*
  Exercises
  1) Crash your program with an OutOfMemoryError
  2) Crash with SOError
  3) PocketCalculator:
     - add(x,y)
     - substract(x,y)
     - multiply(x,y)
     - divide(x,y)
   Throw:
     - OverFlow if add(x,y) exceeds Int.Max_VALUE
     - UnderFlowException if substract(x,y) exceds Int.Min_VALUE
     - MathCalculationException for division by 0
   */

  class OverFlowException extends Exception
  class UnderFlowException extends Exception
  class MathCalculationException extends Exception("Dividing by zero")

  object PocketCalculator {
    def add (x: Int, y: Int): Int = {
      val result = x + y
      if (x > 0 && y > 0 && result < 0) throw new OverFlowException
      else result
    }
    def substract(x: Int, y: Int): Int = {
      val result = x - y
      if (x > 0 && y < 0 && result < 0) throw new OverFlowException
      else if (x < 0 && y > 0 && result > 0) throw new UnderFlowException
      else result
    }
    def multiply(x: Int, y: Int): Int = {
      val result = x * y
      if (x > 0 && y > 0 && result < 0) throw new OverFlowException
      else if (x < 0 && y < 0 && result < 0) throw new OverFlowException
      else if (x > 0 && y < 0 && result > 0) throw new UnderFlowException
      else if (x < 0 && y > 0 && result > 0) throw new UnderFlowException
      else result
    }
    def divide(x: Int, y: Int): Int = {
      if (y == 0) throw new MathCalculationException
      else x / y
    }
  }
  implicit class implicits(i: Int) {
    def print(): Unit = println(i)
  }

  val calculator = PocketCalculator
  calculator.add(Int.MaxValue,10).print()
  calculator.divide(1,0).print()


}