package davidagm
package lectures.part2oop

object Generics extends App {

  class MyList[+A] {
    def add[B >: A](element: B): MyList[B] = {
      /*
      A - Cat
      B -Dog
      MyList[Cat].add(new Dog) => MyList[Animal]
       */
      ???
    }
  }

  val listOfIntegers = new MyList[Int]
  val listOfString = new MyList[String]

  class MyMap[Key, Value] {}

  //generic methods

  object MyList {
    def empty[A]: MyList[A] = ???
  }
  val emptyListOfIntegers = MyList.empty[Int]

  // variance problem
  class Animal
  class Cat extends Animal
  class Dog extends Animal

  // 1) Yes, List[Cat] extends List[Animal] = Covariance
  class CovariantList[+A]
  val animal: Animal = new Cat
  val animalList: CovariantList[Animal] = new CovariantList[Cat]

  // 2) No, List[Cat] does not extends List[Animal]
  class InvariantList[A]
  val invariantAnimalList: InvariantList[Animal] = new InvariantList[Animal]
  //does not compile
  //val invariableCatList: InvariantList[Animal] = new InvariantList[Cat]

  // 3) Can you replace a List[Cat] with List[Animal] => yes. Makes sense? No
  class ContravariantList[-A]
  val contravariantList: ContravariantList[Cat] = new ContravariantList[Animal]
  // Now it makes sense
  class Trainer[-A]
  val trainer: Trainer[Cat] = new Trainer[Animal]

  // bounded types
  class Cage[A <: Animal](animal: A)
  val dogCage = new Cage(new Dog)

  class Car
  // generic type need proper bounded type
  //val carCage = new Cage(new Car)



}
