package davidagm
package lectures.part2oop

object Objets extends App {

  //Scala does not have class-level functionality ("static")
  //Scala object is a SingleTon Instance
  object Person {
    // "static"/"class" - level functionality
    val N_EYES: Int = 2
    def canFly: Boolean = false

    // factoring methods
    //factory method!
    def from(mother: Person, father: Person): Person = new Person("Boobie")
    def apply(mother: Person, father: Person): Person = new Person("Boobie")

  }
  class Person(val name: String) {
    // instance-level functionality
  }


  println(Person.N_EYES)
  println(Person.canFly)

  val booby = Person.from(new Person("Mary"), new Person("John"))
  println(booby.name)
  val alsoBooby = Person(new Person("Mary"), new Person("John"))
  println(alsoBooby.name)
}
