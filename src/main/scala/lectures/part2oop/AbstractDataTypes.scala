package davidagm
package lectures.part2oop

object AbstractDataTypes extends App {

  //Abstract members or classes
  abstract class Animal{
    val creatureType: String
    def eat(): Unit
  }

  class Dog extends Animal {
    // Abstract methods do not need keyword override
    val creatureType: String = "Canine"
    def eat(): Unit = println("Crunch Crunch")
  }

  // Traits - can be inherited across classes
  trait Carnivore {
    val preferredMeal: String = "Fresh meat"
    def eat(animal: Animal): Unit
  }

  class Crocodile extends Animal with Carnivore {
    val creatureType: String = "croc"
    def eat(): Unit = println("nomnomnom")
    def eat(animal: Animal): Unit = println(s"I'm a croc and I'm eating ${animal.creatureType}")
  }

  val dog = new Dog
  val croc = new Crocodile
  croc.eat(dog)
}
