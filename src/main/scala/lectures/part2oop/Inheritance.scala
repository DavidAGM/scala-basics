package davidagm
package lectures.part2oop

object Inheritance extends App {

  val cat = new Cat
  val dog = new Dog
  val dog2 = new Dog2("domestic")
  cat.crunch()

  //Constructor
  // type substitution
  // Polymorphism
  val unknownAnimal: Animal = new Dog2("K9")

  //Single-class inheritance
  //Super-class
  class Animal {
    val creatureType = "wild"

    def eat(): Unit = println("nomnomnom")
  }

  class Cat extends Animal {
    def crunch(): Unit = {
      eat()
      println("crunchcrunch")
    }
  }

  class Person(name: String, age: Int) {
    def this(name: String) = this(name, 0)
  }
  dog.eat()
  println(dog.creatureType)

  //Before you call the constructor of Adult, you MUST pass the parameters of the super class
  class Adult(name: String, age: Int, idCard: String) extends Person(name) {
  }

  //Overriding
  class Dog extends Animal {
    override val creatureType: String = "domestic"

    override def eat(): Unit = println("crunch crunch")
  }
  println(dog2.creatureType)

  class Dog2(override val creatureType: String) extends Animal {
    override def eat(): Unit = println("So tasy, ñam ñam")
  }
  //not accesible
  unknownAnimal.eat()


  //preventing overridies:
  // 1) to use -final- on member
  // 2) to use -final- on class
  // 3) to -seal- the class => you can only extend the class in the file the class is defined

}
