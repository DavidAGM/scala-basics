package davidagm
package lectures.part2oop

import lectures.part1basics.ValuesVariablesTypes.x

object PackagingAndImports {

  // package member are accesible by their simple name
  val writer = new Writer("Daniel", "RockTheJVM", 2021)

  // import the package [import lectures.part1basics.ValuesVariablesTypes.x] or use
  // the fully Qualified name
  val `42` = lectures.part1basics.ValuesVariablesTypes.x
  val `42_` = x

  //package object
  //static values accesible in all the member of the pacakge
  val hello = sayHello()

  // default imports (are there, but hidden)
  //java.lang
  //scala
  //scala.preDef
  //...

}
