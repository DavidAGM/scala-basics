package davidagm
package lectures.part1basics

import scala.annotation.tailrec

object Recursion extends App {


  def factorial(n:Int): Int =
    if (n <= 0) 1
    else {
      println(s"Computing factorial of $n - I first need to compute factorial of ${n-1}")
      val result: Int  = n * factorial(n - 1)
    println(s"Computing factorial of $n")
      result
    }

  //factorial(10000) <- stack overflow

  def anotherFactorial(i: Int): BigInt = {
    @tailrec
    def factorialHelper(x: Int, accumulator: BigInt): BigInt = {
      if (x <= 1) accumulator
      else factorialHelper(x-1, x * accumulator)
    }
  factorialHelper(i, 1)
  }

  println(anotherFactorial(5000))

  @tailrec
  def recursiveConcatenation(s: String, times: Int, result: String = ""): String = {
    if(times <= 0) result
    else recursiveConcatenation(s, times -1, s"$s$result")
  }

  println(recursiveConcatenation("/\\", 3))

  def recursiveIsPrime(n: Int): Boolean = {
    @tailrec
    def stillPrime(t: Int, isStillPrime: Boolean): Boolean = {
      if(!isStillPrime) false
      else if(t <= 1) true
      else stillPrime(t-1, n% t != 0 && isStillPrime)
    }
    stillPrime(n/2, isStillPrime = true)
  }

  println(recursiveIsPrime(17))

  def recursiveFibonacci(n : Int): Int = {
    @tailrec
    def helper(i: Int, last: Int, nextToLast: Int): Int =
      if (i >= n) last
      else helper(i + 1, last + nextToLast , last)
  if(n <= 2) 1
  else helper(2,1,1)
  }

  println(recursiveFibonacci(8))

}
