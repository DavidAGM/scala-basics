package davidagm
package lectures.part1basics

object ValuesVariablesTypes extends App {

  val x: Int = 42
  println(42)

  val aString: String = "hello"
  val anotherString: String = "goodbye"

  val aBoolean: Boolean = false
  val aChar: Char = 'a'
  val aInt: Int = x
  val aShort: Short = 9999
  val aLong: Long = 5454545454545454L
  val aFloat: Float = 2.35f
  val aDouble: Double = 3.141592

  var aVariable: Int = 4
  aVariable = 42


}
