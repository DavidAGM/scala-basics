package davidagm
package lectures.part1basics

object Expressions extends App {

  val x = 1 + 2
  println(x)

  println(2 + 3 * 4)

  println(1 == x)

  println(!(1 == x))
  val aCondition = true
  aVariable += 3
  println(aVariable)
  val aConditionedValue = if (aCondition) 5 else 3
  val aWeirdValue: Unit = {
    aVariable = 3
  }
  println(aConditionedValue)
  val aCodeBlock = {
    val y = 2
    val z = y + 1
    if (z > 2) "hello" else "goodbye"
  }
  var aVariable = 2
  println(aCodeBlock)


}
