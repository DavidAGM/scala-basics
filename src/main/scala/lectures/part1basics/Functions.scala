package davidagm
package lectures.part1basics

import scala.annotation.tailrec

object Functions extends App {

  def aFunction(a: String, b: Int) = s"$a $b"

  println(aFunction("hello", 3))

  def aParameterLessFunction(): Int = 42

  println(aParameterLessFunction())

  def aRepeatedFunction(s: String, n: Int): String = {
    if(n == 1) s
    else s + aRepeatedFunction(s, n - 1)
  }

  println(aRepeatedFunction("hello", 3))

  def aFunctionWithSideEffects(s: String): Unit = println(s)

  aFunctionWithSideEffects("hello World!")

  def aBigFunction(n: Int): Int = {
    def aSmallerFunction(a: Int, b: Int): Int = a + b
    aSmallerFunction(n, n - 1)
  }
  println(aBigFunction(1))

  def greeting(name: String, age: Int): String = s"Hi, my name is $name and I am $age years old"

  println(greeting("Dorota", 33))


  def factorial(n: Int): Int = {
    if (n <= 1) n else n * factorial(n - 1)
  }
  println(factorial(3) + " should be 6")

  def fibonacci(n : Int): Int = {
    if(n <= 2 ) 1
    else fibonacci(n- 1) + fibonacci(n - 2)
  }

  println(s"Fibonacci: ${fibonacci(10)}")



  def isPrime(n: Int): Boolean = {
    @tailrec
    def isPrimeUntil(t: Int): Boolean = if (t <= 1) true else n % t != 0 && isPrimeUntil(t-1)
    isPrimeUntil(n / 2)
  }

  println(isPrime(19))
}
