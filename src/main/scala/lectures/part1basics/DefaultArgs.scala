package davidagm
package lectures.part1basics

import scala.annotation.tailrec

object DefaultArgs {

  @tailrec
  def trFactorial(n: Int, acc: Int = 1): Int =
    if(n <= 1) acc
    else trFactorial(n-1, n*acc)

  def savePicture(format: String = "jpg", width: Int = 800, height: Int = 600): Unit =
    println(format + width + height)

  def main(args: Array[String]): Unit = {
    println(trFactorial(10))
    // It works if yo name them
    savePicture(width = 10, format = "ja", height = 1)

  }

}
