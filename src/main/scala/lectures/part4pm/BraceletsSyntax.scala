package davidagm
package lectures.part4pm

object BraceletsSyntax {

  // 1) if expressions
  val anIfExpression_v1: String = if (2 > 3) "bigger" else "smaller"

  // java-style
  val anIfExpression_v2: String =
    if (2 > 3) {
      "bigger"
    } else {
      "smaller"
    }

  // compact
  val anIfExpression_v3: String =
    if (2 > 3) "bigger"
    else "smaller"

  // scala-3
  val anIfExpression_v4: String =
    if 2 > 3 then
      "bigger"
    else
      "smaller"

  val anIfExpression_v5: String =
    if 2 > 3 then
      val result = "bigger"
      result
    else
      val result = "smaller"
      result

  // scala 3 one-liner
  val anIfExpression_v6: String = if 2 > 3 then "bigger" else "smaller"

  // 2) for comprehensions

  val aForComprehension_v1: List[String] = for {
    n <- List(1, 2, 3)
    s <- List("black", "white")
  } yield s"$n$s"

  val aForComprehension_v2: List[String] =
    for
      n <- List(1, 2, 3)
      s <- List("black", "white")
    yield s"$n$s"

  // 3) Pattern Matching
  val meaningOfLife: Int = 42
  val aPatternMatch_v1: String = meaningOfLife match {
    case 1 => "the one"
    case 2 => "double or nothing"
    case 42 => "meaning of life"
    case _ => "something else"
  }
  val aPatternMatch_v2: String =
    meaningOfLife match
      case 1 => "the one"
      case 2 => "double or nothing"
      case 42 => "meaning of life"
      case _ => "something else"

  // anonymous classes
  val aSpecialAnimal: Animal =
    new Animal:
      override def eat(): Unit = println("I'm special")
      override def grow(): Unit = println("I'm growing very fast!")

  def main(args: Array[String]): Unit = {
    println("If Expressions")
    println(anIfExpression_v1)
    println(anIfExpression_v2)
    println(anIfExpression_v3)
    println(anIfExpression_v4)
    println(anIfExpression_v5)

    println("For Comprehension")
    println(aForComprehension_v1)
    println(aForComprehension_v2)

    println("Pattern Matching")
    println(aPatternMatch_v1)
    println(aPatternMatch_v2)

    println("Methods")
    println(computeMeaningOfLife_v1(1))
    println(computeMeaningOfLife_v2(1))

    println("class / trait / object / ...")
    val animal = new Animal
    animal.eat()
    animal.grow()
    aSpecialAnimal.grow()
    aSpecialAnimal.eat()

  }

  // methods without braces
  def computeMeaningOfLife_v1(arg: Int): Int = {
    val partialResult = 40
    40 + 2
  }

  def computeMeaningOfLife_v2(arg: Int): Int =
    val partialResult = 40
    40 + 2

  // class definition with significan indentation (same for traits, objects, enums, etc)
  class Animal:
    def eat(): Unit = {
      println("I'm eating")
    }

    def grow(): Unit = {
      println("I'm growing")
    } // 1000 more lines of code
  end Animal

}
