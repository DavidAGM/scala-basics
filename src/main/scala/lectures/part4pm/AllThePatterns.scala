package davidagm
package lectures.part4pm

import exercises.MyFunctionalListExercise.{MyFunctionalCons, MyFunctionalEmpty, MyFunctionalList}

import scala.runtime.Nothing$

object AllThePatterns extends App {

  // 1) Constants
  val x: Any = "Scala"
  val constants: String = x match {
    case 1 => "a number"
    case "Scala" => "The Scala"
    case true => "The Truth"
    case AllThePatterns => "A Singleton object"
  }
  println(constants)

  // 2) match anything
  // 2.1) wildcard
  val matchingAnything: Any = x match {
    case _ => x
  }
  println(matchingAnything)
  // 2.2) Variables
  val matchVariable: String = x match {
    case something => s"I've found $something"
  }
  println(matchVariable)

  //3) Tuples
  val aTuple: (Int, Int) = (1, 2)
  val matchATuple: Any = aTuple match {
    case (1, 1) => "Nothing"
    case (something, 2) => s"I've found $something"
  }
  println(matchATuple)

  val nestedTuple: (Int, (Int, Int)) = (1, (2, 3))
  val matchANestedTuple: Any = nestedTuple match {
    case (_, (2, v)) => s"2$v"
  }
  println(matchANestedTuple)

  // 4) Case Classes - constructor pattern
  val aFunctionalList: MyFunctionalList[Int] =
    MyFunctionalCons(1, MyFunctionalCons(2, MyFunctionalEmpty))
  val matchList: Option[Int] = aFunctionalList match {
    case MyFunctionalEmpty => None
    case MyFunctionalCons(head, MyFunctionalEmpty) => Some(head)
    case MyFunctionalCons(head, MyFunctionalCons(subHead, subTail)) => Some(subHead)
  }
  println(matchList)

  // 5) List patterns
  val aStandardList = List(1, 2, 3, 42)
  val standardListMatching: Int = aStandardList match {
    case List(1, _, _, 4) => 1 // extractor - advanced
    case List(2, _*) => 1 // list or arbitrary length - advanced
    case 1 :: List(_) => 1 // infix pattern (prefix)
    case List(1, 2, 3) :+ 42 => 42 // infix patter (suffix)
  }
  println(standardListMatching)

  // 6) Type Specifiers
  val unknown: Any = 2
  val unknownMatch = unknown match {
    case list: List[Int] => list.mkString(";") // explicit type specifier
    case any @ _ =>  s"$any"
  }
  println(unknownMatch)

  // 7) name binding
  val nameBindingMatch = aFunctionalList match {
    case nonEmpty @ MyFunctionalCons(head, tail) => nonEmpty
    case MyFunctionalCons(1, rest @ MyFunctionalCons(2, MyFunctionalEmpty)) => rest
  }
  println(nameBindingMatch)

  // 8) multi pattern
  val multiPattern = aFunctionalList match {
    case MyFunctionalEmpty | MyFunctionalCons(0, _) => 0// compound pattern
    case MyFunctionalCons(1, _) => 1
  }
  println(multiPattern)

  // 9) if guards
  val secondElementSpecial = aFunctionalList match {
    case MyFunctionalCons(_, MyFunctionalCons(specialElement, _)) if specialElement % 2 == 0 => true
    case _ => false
  }
  println(secondElementSpecial)

  // Warning - Type Erasure

  /*
  val res0: List[Int] = List(1)

scala> res0 match {
     | case ls: List[String] => "str"
     | case li: List[Int] => "int"
     | case _ => "other"
     | }
-- Unchecked Warning: ----------------------------------------------------------
2 |case ls: List[String] => "str"
  |     ^
  |     the type test for List[String] cannot be checked at runtime
  */
  val aOptionOfBooleans = Option[Boolean](true)
  val optionMatcher = aOptionOfBooleans match {
    case opStr: Option[String] => opStr
    case opBo: Option[Boolean] => opBo
  }
  println(aOptionOfBooleans)

  val aSeqOfBooleans = Seq[Boolean](true)
  val seqMatcher = aSeqOfBooleans match {
    case seqStr: Seq[String] => seqStr
    case seqBo: Seq[Boolean] => seqBo
  }
  println(aSeqOfBooleans)


  val aListOfInts = List[Int](1,2,3,4,5)
  val listMatcher = aListOfInts match {
    case listStr: List[String] => listStr
    case listBo:  List[Boolean] => listBo
  }
  println(aListOfInts)

}
