package davidagm
package lectures.part4pm

import scala.util.Random

object PatternMatching extends App {

  // switch on steroids
  val random: Random = new Random()
  val x = random.nextInt(10)

  val description = x match {
    case 1 => "the ONE"
    case 2 => "double or nothing"
    case 3 => "third time is the charm"
    case _ => "something else" // _ wildcard
  }

  println(x)
  println(description)

  // 1) Decomposes values
  case class Person(name: String, age: Int)
  val bob: Person = Person("Bob", 20)

  val greeting = bob match {
    // cases are match in order
    case Person(n, a) if a < 21 => s"Hi, my name is $n and I am $a years old and I can't get drunk in the USA"
    case Person(n, a) => s"Hi, my name is $n and I am $a years old"
    case _ => "I don't know who I am"
  }

  val greeting2 = bob match {
    // cases are match in order
    case Person(n, a) => s"Hi, my name is $n and I am $a years old"
    // will never match with the second case because all Person will match the first case
    case Person(n, a) if a < 21 => s"Hi, my name is $n and I am $a years old and I can't get drunk in the USA"
    case _ => "I don't know who I am"
  }

  println(greeting)
  println(greeting2)

  // PM on sealed hierarchies
  sealed class Animal
  case class Dog(breed: String) extends Animal
  case class Parrot(greeting: String) extends Animal
  val animal: Animal = Dog("Terra Nova")
  animal match {
    case Dog(someBreed) => println(s"Matched a dog of the $someBreed breed")
    // if you comment Parrot or Dog, compiler will raise a warning that the patter is not exhaustive (only for sealed)
    case Parrot(greeting) => println(s"Matched a parrot: $greeting")
  }

  //match everything - WHY? - Do not abuse pattern matching!
  val isEven = x match {
    case n if n % 2 == 0 => true
    case _ => false
  }
  val isEvenCond = if(x % 2 == 0) true else false //WHY?
  val isEvenNormal = x % 2 == 0 // Proper implementation

  /* Exercises
  * simple function uses PM takes an Epr => human readable form
  * Sum(Number(2), Number(3)) => 2 + 3
  * same for product and parenthesis
  * */
  trait Expr
  case class Number(n:Int) extends Expr
  case class Sum(e1: Expr, e2: Expr) extends Expr
  case class Prod(e1: Expr, e2: Expr) extends Expr

  // my implementation
  val toHuman: Expr => String = {
    case Number(n) => n.toString
    case Sum(e1, e2) => s"${toHuman(e1)} + ${toHuman(e2)}"
    case Prod(e1, e2) =>
      val addParenthesis: Expr => String = {
        case e@Prod(_, _) => toHuman(e)
        case e@Number(_) => toHuman(e)
        case e => s"(${toHuman(e)})"
      }
      s"${addParenthesis(e1)} * ${addParenthesis(e2)}"

  }
  println(toHuman(Sum(Prod(Number(2), Number(1)), Number(3))))
  println(toHuman(Sum(Prod(Sum(Number(3), Number(2)), Number(4)), Prod(Number(1), Number(2)))))

  // Daniel's
  def show(e: Expr): String = e match {
    case Number(n) => n.toString
    case Sum(e1, e2) => show(e1) + " + " + show(e2)
    case Prod(e1, e2) =>
      def maybeShowParenthesis(expr: Expr) = expr match {
        case Prod(_,_) => show(expr)
        case Number(_) => show(expr)
        case _ => "(" + show(expr) + ")"
      }
      maybeShowParenthesis(e1) + " * " + maybeShowParenthesis(e2)
  }
  println(show(Sum(Prod(Number(2), Number(1)), Number(3))))
  println(show(Sum(Prod(Sum(Number(3), Number(2)), Number(4)), Prod(Number(1), Number(2)))))

}
