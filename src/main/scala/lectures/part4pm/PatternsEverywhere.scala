package davidagm
package lectures.part4pm

object PatternsEverywhere extends App {

  // try - catch are pattern matchers
  try {
    println(10/0)
  } catch {
    case r: RuntimeException => println("runtime")
    case npe: NullPointerException => println("nullPointer")
    case _ => println("something else")
  }

  // for comprehensions use decomposition of case classes and if guards
  val list = List(1,2,3,4,5,6)
  val evenOnes = for {
    x <- list if x % 2 == 0
  } yield 10 * x
println(evenOnes)

  // name biding to assign values to vals
  val tuple = (1,2,3)
  val (a, b, c) = tuple
  println(a)
  println(b)
  println(c)

  // list decomposition also works as name binding
  val head :: tail = list
  println(head)
  println(tail)

  // pattern match as anonymous functions
  val mappedList = list.map {
    case v if v % 2 == 0 => s"$v is even"
    case 1 => "the one"
    case _ => "someting else"
  }
  println(mappedList)
  val mappedList2 = list.map(x => x match {
    case v if v % 2 == 0 => s"$v is even"
    case 1 => "the one"
    case _ => "something else"
  })
  println(mappedList2)
  println(mappedList.equals(mappedList2))

}
