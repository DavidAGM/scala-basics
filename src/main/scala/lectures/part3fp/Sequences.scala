package davidagm
package lectures.part3fp

import scala.util.Random

object Sequences extends App {

  // === Seq ===
  // the constructor Seq() or Range() return List, Vector or other more specific sub-type of Seq

  val seq = Seq(true, false, false, true)
  // prints the seq with implicit toString
  println(seq)
  // reverses the sequence
  println(seq.reverse)
  // calls the apply method of the companion object to retrieve the n-element
  println(seq(2))
  // ++ creates a new sequence joining two sequences
  println(seq ++ Seq(true, true, true))
  // returns an ordered sequence is there is an implicit ordering method for the given type
  println(seq.sorted)

  // === Ranges ===

  // both values inclusive
  val inclusiveRange = 10 to 20
  // last value is excluded from the sequence
  val exclusiveRange = 1 until 10

  // concatenation of sequences
  println(exclusiveRange ++ inclusiveRange)
  // prepend
  println(0 +: exclusiveRange)
  // append
  println(exclusiveRange :+ 10)
  // prepend and append together
  println(0 +: exclusiveRange :+ 10)

  // use ranges as loops
  exclusiveRange.foreach(x => println("I study Scala every day!"))
  exclusiveRange.reverse.foreach(x => println(s"$x days left to achieve mastery of Scala!"))

  // === List ===
  val someList = List('b','c','d','e')
  // prepend and append
  println('a'+:someList:+'f')

  case class Apple(name: String) {
    val proudApple: String = s"I am a proud $name apple!"
  }
  // fill with a value a list of a given length
  val `5applesList` = List.fill(5)(Apple("Golden"))
  println(`5applesList`)
  println(`5applesList`.map((a: Apple) => a.proudApple).mkString("\n"))

  // === Arrays ===
  // it is Java Array

  // there is conversion from Seq type to Array type
  val numbers = Range.inclusive(20,30).toArray
  println(numbers.mkString("Array(", ", ", ")"))

  val fewerNumbers = Array(0,1,2,3)
  println(fewerNumbers.mkString("Array(", ", ", ")"))
  // limited size arrays
  val emptyFewerNumbers = Array.ofDim[Int](4)
  // Int base value is 0
  println(emptyFewerNumbers.mkString("Array(", ", ", ")"))
  // Boolean base value is
  println(Array.ofDim[Boolean](2).mkString("Array(", ", ", ")"))
  // Non primitive Java types are null
  println(Array.ofDim[String](2).mkString("Array(", ", ", ")"))
  println(Array.ofDim[Apple](2).mkString("Array(", ", ", ")"))

  // mutation
  emptyFewerNumbers(0) = 10
  println(emptyFewerNumbers.mkString("Array(", ", ", ")"))

  Range.inclusive(0,3).foreach((i: Int) => emptyFewerNumbers(i) = i)
  println(emptyFewerNumbers.mkString("Array(", ", ", ")"))

  // === Arrays and Sequences ===
  val numberSeq: Seq[Int] = fewerNumbers //implicit conversion
  println(numberSeq) //ArraySeq Type

  // === Vectors ===
  val someVector: Vector[Any] = Vector("haha", 'a', 5, 10L, Apple("Smith"))
  println(someVector)

  // === Vectors vs Lists ===
  val maxRuns: Short = 1000
  val maxCapacity: Int = 10000
  def getWriterTime(collection: Seq[Int]): Double = {
    val r = new Random()
    val times = for {
      run <- 1 to maxRuns
    } yield {
      val currentTime = System.nanoTime()
      collection.updated(r.nextInt(maxCapacity), r.nextInt())
      System.nanoTime() - currentTime
    }
    times.sum.toDouble / maxRuns
  }

  val listTime = getWriterTime(Range.inclusive(0, maxCapacity).toList)
  val vectorTime = getWriterTime(Range.inclusive(0, maxCapacity).toVector)
  println(listTime)
  println(vectorTime)
  val faster = f"${(listTime/vectorTime)*100}%8.2f"
  println(s"Vector is $faster% faster")
}
