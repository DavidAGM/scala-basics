package davidagm
package lectures.part3fp

object MapFlatMapFilterFor extends App {

  val listOfNumbers = List(1, 2, 3, 4)

  // collections like list have head and tail methods
  println(listOfNumbers.head)
  println(listOfNumbers.tail)

  val listOfChars = List('a', 'b', 'c', 'd')

  // map
  println(listOfNumbers.map(_ * -1).map(Math.abs))
  println(listOfNumbers.map((i: Int) => s"$i is a number"))

  // flatMap
  val listOfPlusOne = (x: Int) => List(x, x + 1)
  println(listOfNumbers.flatMap(listOfPlusOne))
  val listOfNegatives = (x: Int) => List(x, x * -1)
  println(listOfNumbers.flatMap(listOfNegatives))

  /*
  Exercises
  1) Print all combinations of listOfNumbers and listOfChars
  */

  println(listOfChars.flatMap((c: Char) => listOfNumbers.map((i: Int) => s"$c$i")))

  // foreach
  listOfNumbers.foreach(println)

  // for-comprehensions
  val forCombinations = for {
    c <- listOfChars
    n <- listOfNumbers
  } yield s"$c$n"

  println(forCombinations)

  for {n <- listOfNumbers} println(n)

  // syntax overload
  listOfNumbers.map { x =>
    x * 2
  }

  /*
  Exercises
  2) MyList support for-comprehensions? --> YES
  
  3) Write a small collection of at most ONE element. MyCollection[+T]:
     - implement map, flatMap and filter
  */

}
