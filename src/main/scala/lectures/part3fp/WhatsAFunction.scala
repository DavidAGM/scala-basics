package davidagm
package lectures.part3fp

object WhatsAFunction extends App {

  // DREAM: Use functions as first class citizens
  // Problem: oop

  val double = new MyFunction[Int, Int] {
    override def apply(element: Int): Int = element * 2
  }

  println(double(2))

  val stringToIntConverter = new MyFunction[String, Int] {
    override def apply(string: String): Int = string.toInt
  }
  println(stringToIntConverter("42"))

  trait MyFunction[A, B] {
    def apply(element: A): B
  }

  /*
  1) function that concatenates 2 string
  2) transfor the MyPredicte and MyTransformer into function types
  3) define a function wich takes an int and returns another function wich
  takes an int and returns an int
   */
  val concat: (String, String) => String = (s1, s2) => s"$s1$s2"
  val superAdder: Int => Int => Int = new Function[Int, Function1[Int, Int]] {
    override def apply(v1: Int): Int => Int = new Function[Int, Int] {
      override def apply(v2: Int): Int = v1 + v2
    }
  }

  println(concat("Hello ", "Scala!"))
  // Curryed function
  println(superAdder(1)(1))

  val specialAdder: Int => Int => Int = v1 => v2 => v1 + v2
  println(specialAdder(1)(1))
}
