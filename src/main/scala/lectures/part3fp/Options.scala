package davidagm
package lectures.part3fp

import scala.util.Random

object Options extends App {

  val myFirstOption: Option[Int] = Some(4)
  val noOption: Option[Int] = None

  println(myFirstOption)

  //unsafe API
  def unsafeMethod(): String = null
  //val result = Some(null) // WRONG
  val result = Option(unsafeMethod())
  println(result)

  // chained methods
  def backupMethod(): String = "A valid result"
  val chainedResult = Option(unsafeMethod()).orElse(Option(backupMethod()))

  // DESIGN unsafe APIs
  def betterUnsafeMethod(): Option[String] = None
  def betterBackupMethod(): Option[String] = Some("a valid value")

  val betterChainedResult = betterUnsafeMethod().orElse(betterBackupMethod())

  // functions on Options
  println(myFirstOption.isEmpty)
  println(myFirstOption.get) // UNSAFE -> Do not use this
  // map, flatMap and filter
  println(myFirstOption.map(_*2)) // Some(8)
  println(noOption.map(_*2))// None
  println(myFirstOption.flatMap(x=> Option(x, x-1))) // Some((4,3))
  println(myFirstOption.filter(x => x > 10)) // None

  // for - comprehensions are available

  /*
  Exercise
  */
  val config: Map[String, String] = Map(
    "host" -> "175.24.26.1",
    "port" -> "22"
  )

  class Connection {
    def connect = "Connected"
  }
  object Connection {
    val random = new Random(System.nanoTime())
    def apply(host: String, port: String): Option[Connection] =
      if (random.nextBoolean()) Some(new Connection)
      else None
  }

  /*
  Try to establish the connection, if so, print the connect method
  */
  val host: Option[String] = config.get("host")
  val port: Option[String] = config.get("port")


  val connection: Option[Connection] = {
    host.flatMap(h => // if host is not null
      port.flatMap(p =>  // if port is not null
        Connection.apply(h, p) // then Some(Connection.apply(host, port))
      )) // else None
  }
  println(connection)
  val connectionStatus: Option[String] = connection.map(_.connect)
  println(connectionStatus)
  connectionStatus.foreach(println)

  // chained calls
  val chainedSolution = {
    config.get("host").flatMap(host => config.get("port").flatMap(port => Connection(host, port))
      .map(connection => connection.connect))
  }
  chainedSolution.foreach(println)

  // for comprehensions
  val connectionComprehension: Option[Connection] = for {
    host <- config.get("host") // Option[String]
    port <- config.get("port") // Option[String]
    // añadimos la construcción dentro para devolver el tipo adecuado
    connection <- Connection.apply(host, port) //Option[Connection]
  } yield connection

  println(connectionComprehension)

}
