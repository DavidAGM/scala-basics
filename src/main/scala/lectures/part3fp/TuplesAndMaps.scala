package davidagm
package lectures.part3fp

import scala.annotation.tailrec

object TuplesAndMaps extends App {

  // === Tuples ===
  val someTuple = (4, 'a')
  println(someTuple._1)
  println(someTuple.copy(_2 = 'b'))
  println(someTuple.swap)

  // === Maps ===

  // key -> value is syntactic sugar for (key, value) tuples
  val someMap: Map[String, Any] = Map("David" -> 'd', "Daniel" -> "l", "42" -> 2)
  println(someMap)
  val anotherMap: Map[String, Float] = Map()
  println(anotherMap)

  val phoneBook: Map[String, Int] = Map(("Jim", 555), ("John", 777))
  println(phoneBook)

  // map operations
  println(phoneBook.contains("Jim"))
  println(phoneBook.contains("Jum"))

  // add paring (tuple)
  val newContact = Map("Mary" -> 999)
  println(newContact)
  // maps are inmutable, so we create a new one
  val newPhoneBook = newContact ++ phoneBook
  println(newPhoneBook)

  // functional on maps - map, flatMap, filter
  println(newPhoneBook.map((x: String, y: Int) => (x.toLowerCase(), y * 100)))
  println(newPhoneBook.flatMap(pair => Map((pair._1.toLowerCase(), pair._2), (pair._1.toUpperCase(), pair._2))))
  println(newPhoneBook.filter(pair => pair._1 == "Mary"))

  // filterKeys
  println(newPhoneBook.view.filterKeys(_.equals("John")).toMap)

  // mapValues
  println(newPhoneBook.view.mapValues((v: Int) => s"+34-$v").toMap)

  // conversion to other collections
  println(phoneBook.toList)
  println(List("Ethan" -> 456, "Sarah" -> 983).toMap)

  // group by
  val nameList = List("Bob", "James", "Angela", "Anthony", "Barbara", "Janine")
  println(nameList.groupBy(name => name.charAt(0)))
  println(newPhoneBook.groupBy(_._1.charAt(0)))
  println(nameList.groupBy((x: String) => x.charAt(1)))
  // we can replace y: Int by `_`because we do not use it
  println(newPhoneBook.groupBy((x: String, _) => x.charAt(1)))

  /*
  Exercise -> Overly simplify social network based on Maps
    - add a person to the network
    - remove a person to the network
    - friend (mutual)
    - unfriend (mutual)
    - numbers of friends per person
    - how many people have NO friends
    - if there is a social connection between two people
  */

  // I added types to make it more readable
  type Person = String // String
  type Friends = Set[Person] // Set[String]
  type Network = Map[Person, Friends] // Map[String, Set[String]]

  def add(network: Network, person: Person): Network = network + (person -> Set())

  def friend(network: Network, a: Person, b: Person): Network = {
    val friendsA: Friends = network(a)
    val friendsB: Friends = network(b)
    network + (a -> (friendsA + b)) + (b -> (friendsB + a))
  }

  def unfriend(network: Network, a: Person, b: Person): Network = {
    val friendsA: Friends = network(a)
    val friendsB: Friends = network(b)
    network + (a -> (friendsA - b)) + (b -> (friendsB - a))
  }

  def remove(network: Network, person: Person): Network = {
    val friends: Friends = network(person)

    @tailrec
    def removeAux(friends: Friends, networkAcc: Network): Network = {
      if (friends.isEmpty) networkAcc
      else removeAux(friends.tail, unfriend(networkAcc, person, friends.head))
    }

    val unFriended: Network = removeAux(friends, network)
    unFriended - person
  }

  val empty: Network = Map()
  val network: Network = add(add(empty, "Bob"), "Mary")
  println(network)
  println(friend(network, "Bob", "Mary"))
  println(unfriend(friend(network, "Bob", "Mary"), "Bob", "Mary"))
  println(remove(friend(network, "Bob", "Mary"), "Bob"))


  def nFriends(network: Network, person: Person): Int = {
    if (network.contains(person)) network(person).size
    else 0
  }
  val testNet: Network = friend(friend(add(network, "Jim"), "Jim", "Bob"), "Bob", "Mary")
  println(testNet)

  def mostFriends(network: Network): Person = {
    network.maxBy((p: Person, f: Friends) => f.size)._1
  }

  def nPeopleWithNoFriends(network: Network): Int = {
    network.count((p: Person, f: Friends) => f.isEmpty)
  }

  println(nFriends(testNet, "Bob"))
  println(mostFriends(testNet))
  println(nPeopleWithNoFriends(testNet))
  println(nPeopleWithNoFriends(unfriend(testNet, "Jim", "Bob")))

  def socialConnection(network: Network, a: Person, b: Person): Boolean = {
    @tailrec
    def bfs(target: Person, consideredPeople: Friends, discoveredPeople: Friends): Boolean = {
      if (discoveredPeople.isEmpty) false
      else {
        val person: Person = discoveredPeople.head
        if (person == target) true
        else if (consideredPeople.contains(person)) bfs(target, consideredPeople, discoveredPeople.tail)
        else bfs(target, consideredPeople + person, discoveredPeople.tail ++ network(person))
      }
    }
    bfs(b, Set(), network(a) + a)
  }

  println(socialConnection(testNet, "Mary", "Jim"))
}
