package davidagm
package lectures.part3fp

import scala.annotation.tailrec

object HOFsAndCurries extends App {

  // This is a High Order Function
  val superFunction:(String, (Int, Boolean => String) => Double) => (Int, Int) = null


  @tailrec
  def nTimes(f: Int => Int, n: Int, x: Int): Int =
    if (n <= 0) x
    else nTimes(f, n-1, f(x))

  val plusOne = (x: Int) => x + 1
  println(nTimes(plusOne, 10, 1))

  def nTimesBetter(f: Int => Int, n: Int): Int => Int =
    if (n <= 0) (x: Int) => x
    else (x: Int) => nTimesBetter(f, n - 1)(f(x))

  val plus10 = nTimesBetter(plusOne, 10)

  println(plus10(2))

  //curried functions
  val supperAdder = (x: Int) => (y: Int) => x+y
  val add3 = supperAdder(3)
  println(add3(2) == 5)
  println(supperAdder(9)(1) == 10)

  //functions with multiple parameter list
  def curriedFormatter(givenFormat: String)(x: Double): String = givenFormat.format(x)

  println(curriedFormatter("%10.8f")(Math.PI))

  val standardFormat = curriedFormatter("%4.2f")
  val precisionFormat = curriedFormatter("%10.8f")

  println(standardFormat(Math.E))
  println(precisionFormat(Math.E))


  /*
  Exercises
  2) Curry
  - def toCurry(f: (A, A) => A): A => A => A
  - def fromCurry(f: A => A => A): (A => A) => A
  3) Composition
  - def compose(f, g): x => f(g(x))
  - def andThen(f,g): x=> g(f(x))
   */

  def toCurry[A](f: (A, A) => A): A => A => A =
    x => y => f(x, y)

  val subtract: (Int, Int) => Int = (x, y) => x - y
  val curriedSubtract = toCurry(subtract)

  println(subtract(10,5))
  println(curriedSubtract(10)(5))

  def fromCurry[A](f: A => A => A): (A, A) => A =
    (x, y) => f(x)(y)

  println(fromCurry(curriedSubtract)(10,5))

  def compose[A, B, T](f: A => B, g: T => A): T => B = x => f(g(x))
  def andThen[A, B, C](f: A => B, g: B => C): A => C = x => g(f(x))

  println(compose[String, String, String](_.toLowerCase(), _.substring(7,10))("Hola, ¿qué tal?"))
  println(compose((x: String) => x.toLowerCase(), (y: String) => y.substring(7,10))("Hola, ¿qué tal?"))
  println(andThen[Int, Int, Int](_ * 2, _ + 1)(10))
  println(andThen[Int, Int, Int](_ + 1, _ * 2)(10))
  println(andThen((x: Int) => x + 1, (y: Int) => y * 2)(10))

}
