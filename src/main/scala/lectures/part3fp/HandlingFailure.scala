package davidagm
package lectures.part3fp

import scala.util.{Failure, Random, Success, Try}

object HandlingFailure extends App {

  //create a success and a failure
  val aSuccess = Success(42)
  val aFailure = Failure(new RuntimeException("Super Failure!"))

  println(aSuccess)
  println(aFailure)

  def unsafeMethod(): String = throw new RuntimeException("No string for you, buster!")

  // Try object via the apply method
  val potentialFailure = Try(unsafeMethod())
  println(potentialFailure)

  // syntactic sugar
  val anotherPotentialFailure = Try {
    //code that might throw
    unsafeMethod()
  }
  println(anotherPotentialFailure)

  // utilities
  println(potentialFailure.isSuccess)

  // orElse
  def backupMethod(): String = "A valid Result"
  val fallbackTry = Try(unsafeMethod()).orElse(Try(backupMethod()))
  println(fallbackTry)

  // IF you design an API
  def betterUnsafeMethod(): Try[String] = Failure(new RuntimeException("Failure"))
  def betterBackupMethod(): Try[String] = Success("A valid result")
  val betterFallback = betterUnsafeMethod() orElse betterBackupMethod()
  println(betterFallback)

  // map, flatMap and filter
  println(aSuccess.map(_*2))
  println(aSuccess.flatMap(x => Success(x*10)))
  println(aSuccess.filter(_ > 10))

  // for comprehensions

  /* Exercise */
  val hostname: String = "localhost"
  val port: String = "8080"
  def renderHTML(page: String): Unit = println(page)

  class Connection {
    def get(url: String): String = {
      val random = new Random(System.nanoTime())
      if (random.nextBoolean()) "<html>...</html>"
      else throw new RuntimeException("Connection interrupted")
    }

    def safeGet(url: String): Try[String] = Try {
      val random = new Random(System.nanoTime())
      if (random.nextBoolean()) "<html>...</html>"
      else throw new RuntimeException("Connection interrupted")
    }

  }

  object HttpService {
    val random = new Random(System.nanoTime())

    def getConnection(host: String, port: String): Connection = {
      if(random.nextBoolean()) new Connection
      else throw new RuntimeException("Someone else took the port")
    }

    def getSafeConnection(host: String, port: String): Try[Connection] = Try {
      if(random.nextBoolean()) new Connection
      else throw new RuntimeException("Someone else took the port")
    }
  }

  // if you get the html page from the connection, print it to the console
  println("== Exercises ==")

  println("Wrapping manually")
  val httpService: Try[Connection] = Try(HttpService.getConnection(hostname, port))
  httpService.map(connection => connection.get("/home")).foreach(page => renderHTML(s"flatMap/map: $page"))

  val htmlForCom: Try[String] = for {
    connection <- Try(HttpService.getConnection(hostname, port))
    html <- Try(connection.get("/home"))
  } yield html
  //We cannot pass directly Try[String] as String to renderHTML
  //renderHTML(htmlForCom) //Won't work

  //if we use forEach/map, it works!
  htmlForCom.foreach(page => renderHTML(s"for-com: $page"))

  println("Using class wrappers")

  for {
    connection <- HttpService.getSafeConnection(hostname, port)
    html <- connection.safeGet("/home")
  } renderHTML(html)
}
