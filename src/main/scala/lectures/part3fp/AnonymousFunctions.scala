package davidagm
package lectures.part3fp

object AnonymousFunctions extends App {

  // anonymous function (LAMBDA)
  val doubler: Int => Int = x => x * 2

  //multiple parameters
  val adder: (Int, Int) => Int = (a, b) => a + b

  //no params
  val justDoSomething: () => Int = () => 3

  // carefull
  println(justDoSomething) // function itself
  println(justDoSomething()) // call

  // curly braces with lambda
  val stringToInt = { (str: String) =>
    str.toInt
  }

  // MOAR syntactic sugar
  val niceIncrementer: Int => Int = _+1//teh same to => (x: Int) => x + 1
  val niceAdder: (Int, Int) => Int = _+_ //(x, y) => x+y


}
