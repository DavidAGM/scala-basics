# scala-basics

Project based on the lectures from the course "Scala & Functional Programming Essentials" by Rock the
JVM ([Daniel Ciocîrlan](https://github.com/rockthejvm)). It contains the code written by Daniel during the videos plus
some of my own implementations and comments.

## Installation

Scala 3 is needed to run this project. To install it, please visit
['Install Scala 3'](https://scala-lang.org/download/scala3.html). Scala 3 is available for Linux, MacOS and Windows

As for an IDE, IntelliJIdea offers support for Scala 3. ['Install IntelliJ](https://www.jetbrains.com/idea/).

## Usage

Every singleton object in the project either extends App or has a main method implemented. You can run the code directly
in the IDE or you can compile it and indicate scala or java witch class you would like them to run.

## Support

If there are some topics that you do not understand well, I recommend watching
the [YouTube channel of Daniel](https://www.youtube.com/c/rockthejvm) or buying some of his courses.

## Roadmap

I will release a Spanish version in another branch. Once done, I am planing on adding new examples in that branch.

## Contributing

If you would like to colaborate by creating more examples and/or adding extra documentation, feel free to contact me.

## Authors and acknowledgment
This project is based on the work of Daniel Ciocîrlan.
Additional code and Spanish language documentation are done by:
- David A. Gil Mendez

## Project status
Work in progress.